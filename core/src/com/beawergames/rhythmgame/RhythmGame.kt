package com.beawergames.rhythmgame

import com.badlogic.gdx.Game
import com.beawergames.rhythmgame.assets.Assets
import com.beawergames.rhythmgame.screens.MenuScreen
import com.beawergames.rhythmgame.util.Prefs
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class RhythmGame: Game() {

	companion object {
		const val PERFECT_DT: Float = (16.67 * 2 / 1000).toFloat()
	}

	private val newLevels = MutableStateFlow(false)
	val updateListener = newLevels.asStateFlow()

	override fun create () {
		Assets.init()
		setScreen(MenuScreen(this))
	}
	
	override fun dispose () {
		Assets.dispose()
	}

	fun setLevels(levels: Array<String>) {
		if(Prefs.saveLevels(levels)) {
			newLevels.update { true }
		}
	}
}
