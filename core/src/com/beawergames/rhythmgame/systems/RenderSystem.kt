package com.beawergames.rhythmgame.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.beawergames.rhythmgame.components.DrawComponent
import com.beawergames.rhythmgame.components.TransformComponent
import kotlin.math.sign
import com.badlogic.gdx.utils.Array

class RenderSystem(private val bath: SpriteBatch, priority: Int):
    IteratingSystem(Family.all(DrawComponent::class.java, TransformComponent::class.java).get(), priority) {

    private val dM = ComponentMapper.getFor(DrawComponent::class.java)
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)

    private val queue = Array<Entity>()

    private val comparator = Comparator<Entity> { p0, p1 -> (tM.get(p0).pos.z - tM.get(p1).pos.z).sign.toInt() }

    override fun update(deltaTime: Float) {
        super.update(deltaTime)

        queue.sort(comparator)

        var dC: DrawComponent?
        var tC: TransformComponent?

        bath.begin()
        for(entity in queue) {
            dC = dM.get(entity)
            tC = tM.get(entity)
            if(dC.tintColor != null) {
                bath.color = dC.tintColor
                bath.draw(dC.textureRegion, tC.pos.x, tC.pos.y, tC.width * 0.5f, tC.height * 0.5f,
                    tC.width , tC.height, tC.scale, tC.scale, tC.angle)
                bath.color = Color.WHITE
            } else bath.draw(dC.textureRegion, tC.pos.x, tC.pos.y, tC.width * 0.5f, tC.height * 0.5f,
                tC.width , tC.height, tC.scale, tC.scale, tC.angle)
        }
        bath.end()

        queue.clear()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        entity?.let { queue.add(it) }
    }
}