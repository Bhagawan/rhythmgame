package com.beawergames.rhythmgame.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.beawergames.rhythmgame.components.MoveComponent
import com.beawergames.rhythmgame.components.TransformComponent

class MoveSystem(priority: Int): IteratingSystem(Family.all(MoveComponent::class.java, TransformComponent::class.java).get(), priority) {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val mM = ComponentMapper.getFor(MoveComponent::class.java)

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val mC = mM.get(entity)
        tC.pos.add(mC.mV.x * deltaTime, mC.mV.y * deltaTime, 0.0f)

    }
}