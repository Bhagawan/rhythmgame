package com.beawergames.rhythmgame.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.beawergames.rhythmgame.assets.Assets
import com.beawergames.rhythmgame.components.EffectComponent

class EffectSystem(private val bath: SpriteBatch, priority: Int):
    IteratingSystem(Family.all(EffectComponent::class.java).get(), priority) {

    private val eM = ComponentMapper.getFor(EffectComponent::class.java)

    override fun update(deltaTime: Float) {
        bath.begin()
        super.update(deltaTime)
        bath.end()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val eC = eM.get(entity)
        if(!eC.effect.isComplete) {
            eC.effect.draw(bath, deltaTime)
        } else {
            Assets.effectPool.free(eC.effect)
            engine.removeEntity(entity)
        }
    }
}