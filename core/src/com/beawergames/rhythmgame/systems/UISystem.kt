package com.beawergames.rhythmgame.systems

import com.badlogic.ashley.core.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.beawergames.rhythmgame.RhythmGame
import com.beawergames.rhythmgame.assets.Assets
import com.beawergames.rhythmgame.assets.Level
import com.beawergames.rhythmgame.assets.Notes
import com.beawergames.rhythmgame.components.*
import com.beawergames.rhythmgame.util.Prefs

class UISystem(private val bath: SpriteBatch, priority: Int, private val level: Level): EntitySystem(priority) {
    private val stage = Stage()
    private lateinit var endScreen :Dialog
    private val one = Gdx.graphics.height / 10
    private var pause = 1000
    private var dT = 0F
    private var cursor = 0

    private val speed = one * 7.0f

    private val shapeRenderer = ShapeRenderer()
    private var trackLength = 0.0f
    private var currentPosition = 0.0f
    private var lives = 5
    private val live = Assets.atlas().createSprite("nota")
    private val wrong = Assets.atlas().createSprite("wrong_img")

    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val sM = ComponentMapper.getFor(SoundComponent::class.java)

    private var uiInterface: UIInterface? = null

    private var running = true

    init {
        getLength()

        live.setSize(50.0f, 50.0f)
        wrong.setSize(50.0f, 50.0f)
    }

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        createUI()
        createEndScreen()
    }

    override fun update(deltaTime: Float) {
        if(running) {
            currentPosition += deltaTime
            dT+= deltaTime * 1000
            if(dT > pause) {
                dT = 0F
                pause = 250
                next()
            }
            checkMissed()
            checkEnd()
        }

        stage.act(Gdx.graphics.deltaTime.coerceAtMost(RhythmGame.PERFECT_DT))
        stage.draw()

        drawUI()
    }

    fun setInterface(uiInterface: UIInterface) {
        this.uiInterface = uiInterface
    }

    fun getStage() : Stage {
        return  stage
    }

    private fun drawUI() {
        val c = Gdx.graphics.width - 2.0f * one
        val percent = if(currentPosition > trackLength) 100.0f else currentPosition * 100 / trackLength
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        shapeRenderer.color = Color.GOLD

        shapeRenderer.rectLine(one.toFloat(), one.toFloat(), one + c / 100 * percent, one.toFloat(), 10.0f)
        shapeRenderer.color = Color.WHITE
        shapeRenderer.rectLine(one + c / 100 * percent, one.toFloat(), one + c, one.toFloat(), 2.0f)
        shapeRenderer.end()

        bath.begin()
        for(i in 5 downTo 1) {
            if(i > lives) {
                wrong.setPosition(Gdx.graphics.width - one - 60.0f * i, Gdx.graphics.height - one - 50.0f)
                wrong.draw(bath)
            } else {
                live.setPosition(Gdx.graphics.width - one - 60.0f * i, Gdx.graphics.height - one - 50.0f)
                live.draw(bath)
            }
        }
        bath.end()
    }

    private fun createUI() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val back = Entity()
        back.add(TransformComponent(0.0f, 0.0f, 0.0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat()))
        back.add(DrawComponent(skin.getRegion("back")))
        engine.addEntity(back)

        val button = Entity()
        button.add(TransformComponent(5.0f, (Gdx.graphics.height - 2.5f * one) / 2.0f , 2.0f, 2.5f * one, 2.5f * one))
        val dC = DrawComponent(Assets.atlas().createSprite("circle2"))
        dC.tintColor = Color(0.8f, 0.46f, 0.11f, 0.8f)
        button.add(dC)
        engine.addEntity(button)

        val line = Entity()
        line.add(TransformComponent(5.0f + one, Gdx.graphics.height / 2.0f, Gdx.graphics.width.toFloat(), 1.0f))
        line.add(DrawComponent(TextureRegion(Assets.color_White)))
        engine.addEntity(line)
    }

    private fun next() {
        if(cursor < level.level.length) {
            when(level.level[cursor]) {
                '\n' -> pause = 2000
                ' ' -> pause = 500
                'A' -> addCircle(Color.PURPLE, Notes.DO)
                'B' -> addCircle(Color.BLUE, Notes.RE)
                'C' -> addCircle(Color.TEAL, Notes.MI)
                'D' -> addCircle(Color.GREEN, Notes.FA)
                'E' -> addCircle(Color.YELLOW, Notes.SOL)
                'F' -> addCircle(Color.ORANGE, Notes.LYA)
                'G' -> addCircle(Color.RED, Notes.SI)
            }
            cursor++
        }
    }

    private fun addCircle(color: Color, nota: Notes) {
        val circle = Entity()
        circle.add(TransformComponent(Gdx.graphics.width - one.toFloat(), (Gdx.graphics.height - one) / 2.0f , one.toFloat(), one.toFloat()))
        val dC = DrawComponent(Assets.assetManager.get("skin/skin.json", Skin::class.java).getRegion("circle"))
        dC.tintColor = color
        circle.add(dC)
        circle.add(SoundComponent(nota))
        circle.add(MoveComponent(-speed, 0.0f))
        engine.addEntity(circle)
    }

    fun press() {
        val sounds = engine?.getEntitiesFor(Family.all(SoundComponent::class.java, TransformComponent::class.java).get())
        var miss = true
        sounds?.let {
            for(sound in it) {
                val sC = sM.get(sound)
                val tC = tM.get(sound)
                if(tC.pos.x > 5 && tC.pos.x + tC.width < 2.5f * one + 5) {
                    sC.sound.play()
                    engine.removeEntity(sound)
                    startEffect(tC.pos.x, tC.pos.y, sC.nota)
                    miss = false
                }
            }
        }
        if(miss) {
            Assets.note_false.play()
            startEffect(one * 2.5f, Gdx.graphics.height / 2 + one * 1.2f, Notes.FALSE)
        }
    }

    private fun startEffect(x: Float, y: Float, nota: Notes) {
        val effect = Entity()
        effect.add(EffectComponent(x, y, nota))
        engine.addEntity(effect)
    }

    private fun getLength() {
        for(c in level.level) {
            when(c) {
                '\n' -> trackLength += 2
                ' ' -> trackLength += 0.5f
                else -> trackLength += 1
            }
        }
    }

    private fun checkMissed() {
        val sounds = engine.getEntitiesFor(Family.all(TransformComponent::class.java, SoundComponent::class.java).get())

        for (sound in sounds) {
            val tC = tM.get(sound)
            if(tC.pos.x < - tC.width) {
                engine.removeEntity(sound)
                lives--
            }
        }
        if(lives <= 0) endGame()
    }

    private fun checkEnd() {
        if(currentPosition >= trackLength) {
            if(engine.getEntitiesFor(Family.all(TransformComponent::class.java, SoundComponent::class.java).get()).size() == 0) {
                val new = when (lives) {
                    5 -> 3
                    in 3..5 -> 2
                    else -> 1
                }
                if(new > level.starAmount ) {
                    level.starAmount = new.toByte()
                    Prefs.saveLevelStars(level)
                }
                showWinScreen(new)
            }
        }
    }

    private fun endGame() {
        endScreen.show(stage)
        running = false
    }

    private fun createEndScreen() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        endScreen = Dialog("", skin)
        endScreen.pad(20.0f)

        val againBtn = TextButton("заново", skin, "gameButton")
        againBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                restart()
                endScreen.hide()
            }
        })
        val exitBtn = TextButton("выход", skin, "gameButton")
        exitBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                uiInterface?.exit()
            }
        })
        endScreen.contentTable.add(Label("Вы проиграли", skin, "defRus", "purple"))
        endScreen.contentTable.row()
        endScreen.button(againBtn).pad(5.0f)
        endScreen.button(exitBtn).pad(5.0f)
    }

    private fun showWinScreen(stars: Int) {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val winScreen = Dialog("", skin)
        winScreen.pad(20.0f)

        val emptyStar = Assets.atlas().createSprite("empty_star")

        val againBtn = TextButton("заново", skin, "gameButton")
        againBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                restart()
                winScreen.hide()
            }
        })
        val exitBtn = TextButton("выход", skin, "gameButton")
        exitBtn.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                uiInterface?.exit()
            }
        })
        winScreen.contentTable.add(Label("Трек окончен", skin, "defRus", "purple")).center().colspan(3)
        winScreen.contentTable.row()

        val firstStar = Image(emptyStar)
        if(stars >= 1) firstStar.color = Color.RED
        winScreen.contentTable.add(firstStar).center()
        val secondStar = Image(emptyStar)
        if(stars >= 2) secondStar.color = Color.RED
        winScreen.contentTable.add(secondStar).center()
        val thirdStar = Image(emptyStar)
        if(stars >= 3) thirdStar.color = Color.RED
        winScreen.contentTable.add(thirdStar).center()
        winScreen.contentTable.row()

        winScreen.button(againBtn).center()
        winScreen.button(exitBtn).center()
        winScreen.show(stage)
        running = false
    }


    interface UIInterface {
        fun exit()
    }

    private fun restart() {
        engine.removeAllEntities()
        createUI()
        cursor = 0
        currentPosition = 0.0f
        pause = 1000
        dT = 0F
        lives = 5
        running = true
    }

}