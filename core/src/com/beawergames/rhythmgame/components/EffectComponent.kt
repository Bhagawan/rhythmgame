package com.beawergames.rhythmgame.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.beawergames.rhythmgame.assets.Assets
import com.beawergames.rhythmgame.assets.Notes

class EffectComponent(x: Float, y: Float, nota: Notes): Component {
    val effect : ParticleEffect = Assets.effectPool.obtain()

    init {
        effect.setPosition(x, y)
        if(nota != Notes.FALSE) effect.emitters.first().sprites = com.badlogic.gdx.utils.Array(arrayOf(Assets.atlas().createSprite("nota")))
        when(nota) {
            Notes.DO -> { effect.emitters[0].start() }
            Notes.RE -> { effect.emitters[1].start() }
            Notes.MI -> { effect.emitters[2].start() }
            Notes.FA -> { effect.emitters[3].start() }
            Notes.SOL -> { effect.emitters[4].start() }
            Notes.LYA -> { effect.emitters[5].start() }
            Notes.SI -> { effect.emitters[6].start() }
            Notes.FALSE -> {
                effect.emitters.first().sprites = com.badlogic.gdx.utils.Array(arrayOf(Assets.atlas().createSprite("wrong_img")))
                effect.emitters.first().start()
            }
        }
    }
}