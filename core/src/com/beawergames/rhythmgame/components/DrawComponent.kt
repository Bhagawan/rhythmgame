package com.beawergames.rhythmgame.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureRegion

class DrawComponent(val textureRegion: TextureRegion): Component {
    var tintColor: Color? = null
}