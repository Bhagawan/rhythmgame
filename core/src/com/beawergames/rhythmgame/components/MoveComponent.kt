package com.beawergames.rhythmgame.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.math.Vector2

class MoveComponent(dX: Float, dY: Float): Component {
    var mV = Vector2(dX, dY)
}