package com.beawergames.rhythmgame.components

import com.badlogic.ashley.core.Component
import com.beawergames.rhythmgame.assets.Assets
import com.beawergames.rhythmgame.assets.Notes

class SoundComponent(val nota: Notes): Component {
    val sound = when(nota) {
        Notes.DO -> { Assets.note_do }
        Notes.RE -> { Assets.note_re }
        Notes.MI -> { Assets.note_mi }
        Notes.FA -> { Assets.note_fa }
        Notes.SOL -> { Assets.note_sol }
        Notes.LYA -> { Assets.note_lya }
        Notes.SI -> { Assets.note_si }
        Notes.FALSE -> { Assets.note_false }
    }
}