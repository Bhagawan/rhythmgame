package com.beawergames.rhythmgame.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.ScreenUtils
import com.beawergames.rhythmgame.RhythmGame
import com.beawergames.rhythmgame.assets.Assets
import com.beawergames.rhythmgame.assets.Level

class MenuScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = GameScreen(game, Level("AAAAAAA", 0))
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(inputAdapter)
        inputMultiplexer.addProcessor(stage)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(RhythmGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        table.background = skin.getDrawable("back")
        stage.addActor(table)

        val btnWidth = Gdx.graphics.width / 3.0f

        val newGameButton = TextButton("Новая Игра", skin,"menuButton")
        newGameButton.addListener(object : ClickListener() {
            override fun touchUp(
                event: InputEvent?,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ) {
                game.screen = LevelChoiceScreen(game)
            }
        })
        table.add(newGameButton).size(btnWidth, 100f).expandY().bottom()
        table.row()

        val exitButton = TextButton("Выход",skin,"menuButton")
        exitButton.addListener( object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                Gdx.app.exit()
            }
        })
        table.add(exitButton).size(btnWidth, 100f).expandY().bottom().padBottom(50.0f)
        table.row()
    }
}
