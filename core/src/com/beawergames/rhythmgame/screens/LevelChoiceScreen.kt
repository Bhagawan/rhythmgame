package com.beawergames.rhythmgame.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.ScreenUtils
import com.beawergames.rhythmgame.RhythmGame
import com.beawergames.rhythmgame.assets.Assets
import com.beawergames.rhythmgame.assets.Level
import com.beawergames.rhythmgame.util.Prefs
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlin.coroutines.EmptyCoroutineContext

class LevelChoiceScreen(val game: Game): ScreenAdapter() {
    private val stage = Stage()
    private var recreateFlag = false

    init {
        createScreen()
        CoroutineScope(EmptyCoroutineContext).launch {
            recreateFlag = true
        }
        CoroutineScope(EmptyCoroutineContext).launch {
            (game as RhythmGame).updateListener.collect {
                if(it) {
                    recreateFlag = true
                    cancel()
                }
            }
        }

    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = GameScreen(game, Level("AAAAAAA", 0))
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(inputAdapter)
        inputMultiplexer.addProcessor(stage)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        if(recreateFlag) {
            recreate()
            recreateFlag = false
        }
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(RhythmGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createScreen() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        table.background = skin.getDrawable("back")
        stage.addActor(table)

        table.pad(50.0f)

        val backButton = TextButton("отмена", skin, "gameButton")
        backButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                game.screen = MenuScreen(game)
            }
        })

        val recordGroup = VerticalGroup()
        val scroll = ScrollPane(recordGroup)
        scroll.setFlickScroll(true)
        table.add(backButton).expand().bottom()
        table.center().add(scroll).expand().top()
        table.add().expandX()
        recordGroup.space(10.0f)

        val levels = Prefs.getLevels()
        val emptyStar = Assets.atlas().createSprite("empty_star")

        for(level in levels.withIndex()) {
            val lGroup = Table()
            val name = Label("Уровень ${level.index + 1}", skin)
            lGroup.add(name).expandX().pad(50.0f)

            val firstStar = Image(emptyStar)
            if(level.value.starAmount >= 3) firstStar.color = skin.getColor("gold")
            firstStar.setOrigin(Align.center)
            firstStar.rotation = -45.0f
            lGroup.add(firstStar)

            val secondStar = Image(emptyStar)
            if(level.value.starAmount >= 2) secondStar.color = skin.getColor("gold")
            lGroup.add(secondStar).padBottom(5.0f)

            val thirdStar = Image(emptyStar)
            if(level.value.starAmount >= 1) thirdStar.color= skin.getColor("gold")
            thirdStar.setOrigin(Align.center)
            thirdStar.rotation = 45.0f
            lGroup.add(thirdStar).padRight(50.0f)

            lGroup.background = skin.getDrawable("purple_texture")

            lGroup.touchable = Touchable.enabled
            lGroup.addListener( object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    game.screen = GameScreen(game, level.value)
                }
            })


            val cont = Container(lGroup)
            cont.background = skin.getDrawable("gold_texture")
            cont.pad(3.0f)
            recordGroup.addActor(cont)
        }

    }

    private fun recreate() {
        stage.clear()
        createScreen()
    }
}