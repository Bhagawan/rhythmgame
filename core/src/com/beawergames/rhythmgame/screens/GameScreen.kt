package com.beawergames.rhythmgame.screens

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.ScreenUtils
import com.beawergames.rhythmgame.assets.Assets
import com.beawergames.rhythmgame.assets.Level
import com.beawergames.rhythmgame.systems.EffectSystem
import com.beawergames.rhythmgame.systems.MoveSystem
import com.beawergames.rhythmgame.systems.RenderSystem
import com.beawergames.rhythmgame.systems.UISystem

class GameScreen(private val game: Game, level: Level): ScreenAdapter() {
    private var batch = SpriteBatch()
    private val camera = OrthographicCamera()
    private val engine = Engine()

    private val uiSystem = UISystem(batch,120, level)
    init {
        Assets.init()
        camera.setToOrtho(false)

        uiSystem.setInterface(object: UISystem.UIInterface {
            override fun exit() {
                game.screen = MenuScreen(game)
            }
        })

        engine.addSystem(uiSystem)
        engine.addSystem(MoveSystem(50))
        engine.addSystem(RenderSystem(batch, 100))
        engine.addSystem(EffectSystem(batch, 101))
    }

    override fun show() {
        Gdx.input.setCatchKey(Input.Keys.BACK, true)
        val inputAdapter  = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = MenuScreen(game)
                    true
                }
                Input.Keys.BACK -> {
                    game.screen = MenuScreen(game)
                    true
                }
                else -> false
            }

            override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                uiSystem.press()
                return true
            }

            override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {

                return true
            }

            override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {

                return true
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(uiSystem.getStage())
        inputMultiplexer.addProcessor(inputAdapter)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0f, 0f, 0f, 1f)
        batch.projectionMatrix = camera.combined
        camera.update()

        engine.update(delta)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun dispose() {
        batch.dispose()
    }
}