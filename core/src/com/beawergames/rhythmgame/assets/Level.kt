package com.beawergames.rhythmgame.assets

data class Level(val level: String, var starAmount: Byte) {
    constructor(): this("", 0)
}

