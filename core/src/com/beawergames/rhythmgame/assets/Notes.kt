package com.beawergames.rhythmgame.assets

enum class Notes {
    DO, RE, MI, FA, SOL, LYA, SI, FALSE
}