package com.beawergames.rhythmgame.assets

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.beawergames.rhythmgame.util.MyPool

class Assets {
    companion object {
        val assetManager = AssetManager()
        lateinit var effectPool : MyPool

        lateinit var note_do: Sound
        lateinit var note_re: Sound
        lateinit var note_mi: Sound
        lateinit var note_fa: Sound
        lateinit var note_sol: Sound
        lateinit var note_lya: Sound
        lateinit var note_si: Sound
        lateinit var note_false: Sound

        lateinit var color_White: Texture

        fun init() {
            assetManager.load("skin/skin.atlas", TextureAtlas::class.java)
            assetManager.load("skin/MazzardSoft.fnt", BitmapFont::class.java)
            assetManager.load("skin/skin.json", Skin::class.java)

            val partParameter = ParticleEffectLoader.ParticleEffectParameter()
            partParameter.atlasFile = "skin/skin.atlas"
            assetManager.load("effect_press", ParticleEffect::class.java, partParameter)

            while (!assetManager.update()) {}

            effectPool = MyPool((assetManager.get("effect_press", ParticleEffect::class.java)), 1, 10)

            note_do = loadSound("notes/do.mp3")
            note_re = loadSound("notes/re.mp3")
            note_mi = loadSound("notes/mi.mp3")
            note_fa = loadSound("notes/fa.mp3")
            note_sol = loadSound("notes/sol.mp3")
            note_lya = loadSound("notes/lya.mp3")
            note_si = loadSound("notes/si.mp3")
            note_false = loadSound("wrong_sound.mp3")

            createColors()
        }

        fun dispose() {
            assetManager.dispose()
        }

        fun atlas() : TextureAtlas = assetManager.get("skin/skin.atlas", TextureAtlas::class.java)

        private fun loadSound(file: String?): Sound {
            return Gdx.audio.newSound(Gdx.files.internal(file))
        }

        private fun createColors() {
            val pixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
            pixmap.drawPixel(0,0,Color.rgba8888(1.0f,1.0f,1.0f,1.0f))
            color_White = Texture(pixmap)
            pixmap.dispose()
        }
    }
}