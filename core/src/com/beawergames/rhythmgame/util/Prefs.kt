package com.beawergames.rhythmgame.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json
import com.beawergames.rhythmgame.assets.Level

class Prefs {
    companion object {
        fun saveLevels(levels: Array<String>): Boolean {
            var out = false
            val prefs = Gdx.app.getPreferences("Levels")
            val j = Json()
            var savedLevels = j.fromJson(Array<Level>::class.java, prefs.getString("levels", "[]"))
            for(level in levels) {
                var add = true
                for(sL in savedLevels) if(sL.level == level) add = false
                if(add) {
                    savedLevels = savedLevels.plusElement(Level(level, 0))
                    out = true
                }
            }
            prefs.putString("levels", j.toJson(savedLevels))
            prefs.flush()
            return out
        }

        fun getLevels(): Array<Level> {
            val prefs = Gdx.app.getPreferences("Levels")
            val r = prefs.getString("levels", "[]")
            return Json().fromJson(Array<Level>::class.java, r)
        }

        fun saveLevelStars(changedLevel: Level) {
            val prefs = Gdx.app.getPreferences("Levels")
            val j = Json()
            val savedLevels = j.fromJson(Array<Level>::class.java, prefs.getString("levels", "[]"))

            for(sL in savedLevels) if(sL.level == changedLevel.level) sL.starAmount = changedLevel.starAmount

            prefs.putString("levels", j.toJson(savedLevels))
            prefs.flush()
        }
    }
}