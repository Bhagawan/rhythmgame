package com.beawergames.rhythmgame.util

import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.utils.Pool

class MyPool(private val effect: ParticleEffect,initialCapacity: Int,max: Int): Pool<ParticleEffect>(initialCapacity, max) {

    override fun newObject(): ParticleEffect {
        return ParticleEffect(effect)
    }
}