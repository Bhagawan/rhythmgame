package com.beawergames.rhythmgame.util

import androidx.annotation.Keep

@Keep
class RhythmSplashResponse(val url : String)