package com.beawergames.rhythmgame.network

import com.beawergames.rhythmgame.util.RhythmSplashResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface RhythmServerClient {

    @FormUrlEncoded
    @POST("RhythmGame/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Response<RhythmSplashResponse>

    @GET("RhythmGame/melodies.json")
    suspend fun getMelodies(): Response<Array<String>>

    companion object {
        fun create() : RhythmServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(RhythmServerClient::class.java)
        }
    }

}