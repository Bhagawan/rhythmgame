package com.beawergames.rhythmgame

import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.beawergames.rhythmgame.network.RhythmServerClient
import kotlinx.coroutines.*

class AndroidLauncher: AndroidApplication() {
	private val game = RhythmGame()
	private lateinit var request: Job

	override fun onCreate (savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val config = AndroidApplicationConfiguration()
		config.useImmersiveMode = true
		initialize(game, config)

		request = CoroutineScope(Dispatchers.IO).async {
			val r = RhythmServerClient.create().getMelodies()
			if(r.isSuccessful) {
				r.body()?.let { game.setLevels(it) }
			}
		}
	}

	override fun onDestroy() {
		super.onDestroy()
		if(request.isActive) request.cancel()
	}
}
